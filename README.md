## Nginx + MariaDB + MailHog + Redis + PHP-74 FPM + PHPMYADMIN + ADMINER

### Management
List Command In MakeFile for easer instead `docker-compose` by `make`
for windown
`
http://gnuwin32.sourceforge.net/packages/make.html
`
download and install make for windown

for windown 
http://gnuwin32.sourceforge.net/packages/make.htm

#### First run:

```
cp .env.example .env
#edit .env 

cd ../ 
mkdir projects  
# go out root docker folder and create projects folder for mount volume

#you can choose the template with specific php version
make up

make exec name=php-74
composer create-project --prefer-dist laravel/laravel blog
change .env file
DB_HOST=db
REDIS_HOST=redis
# name of service docker-compose 
```

#### Run:

```
#docker-compose up -d
make up
```

#### Stop

```
#docker-compose stop
make stop
```


####  View the status of running containers

```
#docker-compose ps
make ps
```


#### Viewing container logs

```
#docker-compose logs -tail=100 -f (php-74|db|mailhog|nginx)
make logs name=php-74
```


#### After launch, services are available at:

http://localhost:8025 - mailhog

http://localhost:8080 - phpmyadmin (root/123456) change root password in .env

http://localhost:8081 - adminer

http://site.test - test site (you need to add site.test to hosts file) 

*Database host - db*

### Fine tuning


#### Host user permissions

In the terminal, using the id command, we get the digital identifier of our user and group.
Then uncomment the line

```
#RUN usermod -u 1050 www-data && groupmod -g 1050 www-data
```

In docker /*php*/build/Dockerfile and replace 1050 with your identifiers there.
We start containers with a rebuild

```
#docker-compose up -d --build
make upb
```


#### php.ini settings

Open docker/php-74/config/php.ini
Or edit the php-fpm settings - www.conf

#### Connect to the database from the console

```
#docker-compose -f docker-compose.mycli.yml run --rm mycli /bin/ash -c "mycli -uroot -hdb -p\$$MYSQL_ROOT_PASSWORD" || true
make mycli
```


#### Run php scripts from the console

```
#docker-compose exec $(name) /bin/sh || true
make exec name=php-74
```


#### Route access to the database

The password is registered in the MYSQL_ROOT_PASSWORD parameter in .env


#### Change of database access details

Changed in .env file


#### Acme.sh

```
#docker-compose -f docker-compose.acme.yml run --rm acme acme.sh --issue -d `echo $(d) | sed 's/,/ \-d /g'` -w /acme-challenge
make ssl d="site.ru,www.site.ru"
```
SSL certificates are saved in the docker/nginx/ssl directory. To make it work you need to uncomment
lines in the docker-compose.yml config

```
      # - ./docker/nginx/ssl:/etc/nginx/ssl:ro
```

Also uncomment the line

```
      # -"443:443"
```

*crontab*

```
00 3 * * * /usr/local/bin/docker-compose -f /srv/www/docker-compose-php/docker-compose.acme.yml run --rm acme acme.sh --cron
02 3 * * * /usr/local/bin/docker-compose -f /srv/www/docker-compose-php/docker-compose.yml exec -T nginx nginx -t -q && /usr/local/bin/docker-compose -f /srv/www/docker-compose-php/docker-compose.yml restart nginx
```

If you need to run acme.sh for some other purpose, you can do this with this command:

```
make acme
```

#### Node.js

```
#docker-compose -f docker-compose.node.yml run --rm node-10 /bin/ash || true
make node
```

